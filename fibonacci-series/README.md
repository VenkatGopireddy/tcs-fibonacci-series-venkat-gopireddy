# Table of Contents in Creating Fibonacci Series App

## Fibonacci Series

- Basic Initial App Set Up
- App Scaffolding
- Adding a touch of Style
- Installing peer dependencies
- Connecting Redux Form
- Building Number Input Component
  - Decorationg reduxForm method
  - Placing Form Field
  - Customizing Form Field & Handling Form Submission
  - Validating the Field
  - Passing value from Child Component to Parent Component
  - Filtering Fibonacci Series Value
- Passing Value from Parent Component to Child Component
- Building Number List Component
  - Receiving props from Parent
  - Rendering Fibonnacci Series
  - Validating either it is a Prime or Composite Number
  - Styling so 'tic' is printed in blue, and 'toe' in green
  - Pagination & Styling
  - Substitute tic and toe with wic & woe as it is Saturday & Sunday
  - Code Clean Up

## Unit Testing

- Enzyme Set Up
- Configuring Enzyme Adapter & Writing a Valid Test for the App Component with Test Structure
- Adding Valid Tests to the Other Components