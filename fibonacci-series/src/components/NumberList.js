import React, { Component } from 'react';

import * as copy from '../Utils/constants';
import './style.css';

class NumberList extends Component {
  state = {
    currentPage: 1,
    fibSeriesPerPage: 10,
  };

  handleClick = (event) => {
    this.setState({
      currentPage: Number(event.target.id),
    });
  };

  render() {
    const { fibSeries, weekDay } = this.props;

    const { currentPage, fibSeriesPerPage } = this.state;
    const indexOfLastFib = currentPage * fibSeriesPerPage;
    const indexOfFirstFib = indexOfLastFib - fibSeriesPerPage;
    const currentFibSeries = fibSeries.slice(indexOfFirstFib, indexOfLastFib);

    const renderFibSeries = currentFibSeries.map((fibNum, index) => {
      return (
        <div
          className="ui four column doubling stackable grid container"
          key={index}
        >
          <div className="column">
            <p>
              <span>{fibNum}</span>
            </p>
          </div>
          {isPrime(fibNum) ? (
            <div className="column">
              <p>
                <span className="tic">{weekDay.prime}</span>
              </p>
            </div>
          ) : (
            <div className="column">
              <p>
                <span className="toe">{weekDay.compo}</span>
              </p>
            </div>
          )}
        </div>
      );
    });

    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(fibSeries.length / fibSeriesPerPage); i++) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.map((number) => {
      return (
        <li key={number} id={number} onClick={this.handleClick}>
          {number}
        </li>
      );
    });

    return (
      <div className="ui container">
        <div>
          <h3 className="ui header">{copy.fibonacciList}</h3>
          {renderFibSeries}
        </div>
        <hr />
        <div>
          <h4 className="ui header">{copy.pagination}</h4>
          <ul className="page-numbers">{renderPageNumbers}</ul>
        </div>
        <hr />
      </div>
    );
  }
}

const isPrime = (num) => {
  for (let i = 2; i < num; i++) if (num % i === 0) return false;
  return num > 1;
};

export default NumberList;
