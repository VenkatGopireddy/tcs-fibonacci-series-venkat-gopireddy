import React from 'react';

import FibonacciOutput from './FibonacciOutput';

const App = () => {
  return (
    <div>
      <FibonacciOutput />
    </div>
  );
};

export default App;
