import React, { Component } from 'react';

import NumberInput from './NumberInput';
import NumberList from './NumberList';

class FibonacciOutput extends Component {
  state = {
    fibSeries: [],
    weekDay: { prime: 'tic', compo: 'toe' },
  };

  handleInput = (fibSeries) => {
    this.setState({ fibSeries });

    const today = new Date();
    if (today.getDay() === 6 || today.getDay() === 0) {
      // To Test Uncomment the below and comment the above if it is not a weekEnd
      // if ((today.getDay() === today.getDay())) {
      this.setState({ weekDay: { prime: 'wic', compo: 'woe' } });
    }
  };

  render() {
    const { fibSeries, weekDay } = this.state;
    return (
      <div style={{ margin: 30 }}>
        <NumberInput getFibValue={this.handleInput} />
        <NumberList fibSeries={fibSeries} weekDay={weekDay} />
      </div>
    );
  }
}

export default FibonacciOutput;
