import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';

import * as copy from '../Utils/constants';

const required = (value) => (value ? undefined : 'Required');
const number = (value) =>
  value && isNaN(Number(value)) ? 'Must be a number' : undefined;

class NumberInput extends Component {
  state = {
    enteredInput: '',
    fibFilterValue: 18,
  };

  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="ui error message">
          <div className="header">{error}</div>
        </div>
      );
    }
  }

  renderInput = ({ input, label, meta }) => {
    const className = `field ${meta.error && meta.touched ? 'erorr' : ''}`;
    return (
      <div className={className}>
        <label>{label}</label>
        <input {...input} autoComplete="off" />
        {this.renderError(meta)}
      </div>
    );
  };

  filterFibSeries = (element) => {
    const { enteredInput } = this.state;
    return element <= enteredInput;
  };

  onSubmit = (value) => {
    const { fibFilterValue } = this.state;
    const fibSeries = fibonacci(fibFilterValue);
    const fibSeriesResult = fibSeries.filter(this.filterFibSeries);

    this.props.getFibValue(fibSeriesResult);
  };

  render() {
    return (
      <div className="ui container">
        <h2>{copy.fibonacciSeries}</h2>
        <hr />
        <form
          className="ui form error"
          onSubmit={this.props.handleSubmit(this.onSubmit)}
        >
          <Field
            name="numberInput"
            type="number"
            component={this.renderInput}
            label="Enter Number"
            validate={[required, number]}
            onChange={(e) => this.setState({ enteredInput: e.target.value })}
          />
          <button className="ui button primary">{copy.submit}</button>
        </form>
        <hr />
      </div>
    );
  }
}

const fibonacci = (n) =>
  Array.from({ length: n }).reduce(
    (acc, val, i) => acc.concat(i > 1 ? acc[i - 1] + acc[i - 2] : i),
    []
  );

export default reduxForm({ form: 'numberInput' })(NumberInput);
