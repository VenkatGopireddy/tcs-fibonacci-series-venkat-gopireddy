import React from 'react';
import { shallow } from 'enzyme';

import Fibonacci from '../components/FibonacciOutput';
import NumberInput from '../components/NumberInput';
import NumberList from '../components/NumberList';

describe('Fibonacci', () => {
  const fibonacci = shallow(<Fibonacci />);

  it('contain the NumberInput component', () => {
    expect(fibonacci.find(NumberInput).exists()).toBe(true);
  });

  it('contain the NumberList component', () => {
    expect(fibonacci.find(NumberList).exists()).toBe(true);
  });

  it('initializes the `state` with an empty list of fibSeries ', () => {
    expect(fibonacci.state().fibSeries).toEqual([]);
  });

  it('initializes the `state` with default value of weekDay ', () => {
    expect(fibonacci.state().weekDay).toEqual({ prime: 'tic', compo: 'toe' });
  });
});
