import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import NumberInput from '../components/NumberInput';
import reducers from '../reducers/index';

const store = createStore(reducers);

describe('NumberInput', () => {
  let numberInput;

  beforeEach(() => {
    numberInput = mount(
      <Provider store={store}>
        <NumberInput />
      </Provider>
    );
  });

  afterEach(() => {
    numberInput.unmount();
  });

  it('contain the NumberInput component', () => {
    expect(numberInput.find('NumberInput').exists()).toBe(true);
  });

  it('has a Field in the component', () => {
    expect(numberInput.find('Field').exists()).toBe(true);
  });

  it('has a button in the component', () => {
    expect(numberInput.find('button').length).toEqual(1);
  });

  describe('renders the text', () => {
    it('renders Fibonacci Series text', () => {
      expect(numberInput.render().text()).toContain('Fibonacci Series');
    });

    it('renders Enter Number text', () => {
      expect(numberInput.render().text()).toContain('Enter Number');
    });
  });
});
