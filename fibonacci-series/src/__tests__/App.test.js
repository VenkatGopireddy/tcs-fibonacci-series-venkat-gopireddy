import React from 'react';
import { shallow } from 'enzyme';

import App from '../components/App';

describe('App', () => {
  const app = shallow(<App />);

  it('contain the Fibonacci component', () => {
    expect(app.find('FibonacciOutput').exists()).toBe(true);
  });
});
